Module circulant_solver_module

  Public :: sten_to_list
  Public :: calc_pseudo_inv_diag
  Public :: calc_pseudo_inv_ord
  Public :: calc_pseudo_inv_perm
  Public :: calc_pseudo_inv_perm_mpi

  Private
  
Contains

  Pure Subroutine sten_to_list( n_mu, lo, sten, n_c, i_c, c, w_c )

    ! Need to sort out properly!!
    ! BORKEN IF LAST DIM = 1
    ! Gives some zero weights, and get rid of stupid inner loop

    Use numbers_module, Only : wp
    
    Implicit None ( Type, External )

    Integer   , Dimension( 1:3                          ),              Intent( In    ) :: n_mu
    Integer   , Dimension( 1:3                          ),              Intent( In    ) :: lo
    Real( wp ), Dimension( lo( 1 ):, lo( 2 ):, lo( 3 ): ),              Intent( In    ) :: sten
    Integer                                              ,              Intent(   Out ) :: n_c
    Integer   , Dimension( :                            ), Allocatable, Intent(   Out ) :: i_c
    Real( wp ), Dimension( :                            ), Allocatable, Intent(   Out ) :: c
    Real( wp ), Dimension( :                            ), Allocatable, Intent(   Out ) :: w_c

    Integer :: ntot
    Integer :: i_sten
    Integer :: ix, iy, iz
    Integer :: i
    
    ntot = Product( n_mu )
    
    Allocate( c( 0:0 ) )
    c = [ sten( 0, 0, 0 ) ]
    Allocate( i_c( 1:0 ) )
    Allocate( w_c( 1:0 ) )
    
    n_c = 0
    Do iz = Lbound( sten, Dim = 3 ), Ubound( sten, Dim = 3 )
       Do iy = Lbound( sten, Dim = 2 ), Ubound( sten, Dim = 2 )
          ix_loop: Do ix = Lbound( sten, Dim = 1 ), Ubound( sten, Dim = 1 )
             If( ix == 0 .And. iy == 0 .And. iz == 0  ) Cycle
             If( Abs( sten( ix, iy, iz ) ) < 1e-12_wp ) Cycle
             n_c = n_c + 1
             i_sten = ix + n_mu( 1 ) * iy + n_mu( 1 ) * n_mu( 2 ) * iz
             i_sten = Modulo( i_sten, Product( n_mu ) )
             i_c = [ i_c, i_sten ]
             c = [ c, sten( ix, iy, iz ) ]
             Do i = 1, Size( i_c ) - 1
                If( i_c( i ) == i_sten ) Then
                   w_c = [ w_c, 0.0_wp ]
                   Cycle ix_loop
                End If
             End Do
             If( i_sten > ntot / 2 ) Then
                w_c = [ w_c, 0.0_wp ]
             Else
                If( i_sten /= ntot / 2 .Or. Mod( ntot, 2 ) /= 0 ) Then
                   w_c = [ w_c, 2.0_wp ]
                Else
                   w_c = [ w_c, 1.0_wp ]
                End If
             End If
          End Do ix_loop
       End Do
    End Do
    
  End Subroutine sten_to_list

  Subroutine calc_pseudo_inv_diag( n, n_c, i_c, c, a_inv )

    Use numbers_module, Only : wp
    
    Implicit None ( Type, External )

    Integer                        , Intent( In    ) :: n
    Integer                        , Intent( In    ) :: n_c
    Integer   , Dimension( 1:     ), Intent( In    ) :: i_c
    Real( wp ), Dimension( 0:     ), Intent( In    ) :: c
    Real( wp ), Dimension( 0:,  : ), Intent(   Out ) :: a_inv

    Interface
       Subroutine dsyev( jobz, uplo, n, a, lda, w, work, lwork, info )
         Import :: wp
         Character                          , Intent( In    ) :: jobz
         Character                          , Intent( In    ) :: uplo
         Integer                            , Intent( In    ) :: n
         Integer                            , Intent( In    ) :: lda
         Real( wp ), Dimension( 1:lda, 1:* ), Intent( InOut ) :: a
         Real( wp ), Dimension( 1:*        ), Intent(   Out ) :: w
         Real( wp ), Dimension( 1:*        ), Intent( InOut ) :: work
         Integer                            , Intent( In    ) :: lwork
         Integer                            , Intent(   Out ) :: info
       End Subroutine dsyev
       Subroutine dgemm( transa, transb, m, n, k, alpha, a, lda, b, ldb, beta, c, ldc )
         Import :: wp
         Character                          , Intent( In    ) :: transa
         Character                          , Intent( In    ) :: transb       
         Integer                            , Intent( In    ) :: m
         Integer                            , Intent( In    ) :: n
         Integer                            , Intent( In    ) :: k
         Real( wp )                         , Intent( In    ) :: alpha
         Real( wp ), Dimension( 1:lda, 1:* ), Intent( In    ) :: a
         Integer                            , Intent( In    ) :: lda
         Real( wp ), Dimension( 1:ldb, 1:* ), Intent( In    ) :: b
         Integer                            , Intent( In    ) :: ldb
         Real( wp )                         , Intent( In    ) :: beta
         Real( wp ), Dimension( 1:ldc, 1:* ), Intent(   Out ) :: c
         Integer                            , Intent( In    ) :: ldc         
       End Subroutine dgemm
    End Interface

    Real( wp ), Dimension( :, : ), Allocatable :: a
    Real( wp ), Dimension( :, : ), Allocatable :: q
    Real( wp ), Dimension( :, : ), Allocatable :: q_scaled

    Real( wp ), Dimension( : ), Allocatable :: e
    Real( wp ), Dimension( : ), Allocatable :: work

    Real( wp ) :: e_inv
    
    Integer :: info
    Integer :: i

    Allocate( a( 0:n - 1, 0:n - 1 ) )

    a(  0, 0 ) = c( 0 )
    a( 1:, 0 ) = 0.0_wp
    Do i = 1, n_c
       a(     i_c( i ), 0 ) = c( i )
       a( n - i_c( i ), 0 ) = c( i )
    End Do
    Do i = 1, n - 1
       a( :, i ) = Cshift( a( :, 0 ), -i )
    End Do

    Allocate( q, Source = a )
    Allocate( e( 0:n - 1 ) )
    Allocate( work( 1:200 * 3 * n ) )
    Call dsyev( 'V', 'U', n, q, Size( q, Dim = 1 ), e, work, Size( work ), info )

    Allocate( q_scaled, Mold = q )
    Do i = 1, n - 1
       e_inv = 1.0_wp / e( i )
       q_scaled( :, i ) = q( :, i ) * e_inv
    End Do

    Call dgemm( 'N', 'T', n, n, n - 1, 1.0_wp, q_scaled( 0, 1 ), n, &
                                               q( 0, 1 )       , n, &
                                       0.0_wp, a_inv           , Size( a_inv, Dim = 1 ) )
    
  End Subroutine calc_pseudo_inv_diag
  
  Subroutine calc_pseudo_inv_ord( n, i_c, c, w_c, a_inv )

    Use numbers_module, Only : wp
    
    Implicit None ( Type, External )

    Integer                         , Intent( In    ) :: n
    Integer   , Dimension( 1:      ), Intent( In    ) :: i_c
    Real( wp ), Dimension( 0:      ), Intent( In    ) :: c
    Real( wp ), Dimension( 1:      ), Intent( In    ) :: w_c
    Real( wp ), Dimension( 0:,  0: ), Intent(   Out ) :: a_inv

    Real( wp ), Dimension( : ), Allocatable :: omega
    Real( wp ), Dimension( : ), Allocatable :: q_scaled
    Real( wp ), Dimension( : ), Allocatable :: a_row

    Integer :: n_top, n_max
    Integer :: i_state, j
    
    !$omp parallel default( none ) shared( n, i_c, c, w_c, a_inv, q_scaled, a_row ) &
    !$omp&                         private( omega, n_top, n_max, j, i_state )
    
    ! Limits for even and odd cases
    If( Mod( n, 2 )  == 0 ) Then
       n_top = ( n - 2 ) / 2 ! Number of pairs of degenerate evals excluding zero eval
       n_max = n_top + 1     ! Number of unique evals - for even last eval is not degenerate
    Else
       n_top = ( n - 1 ) / 2 ! Number of pairs of degenerate evals excluding zero eval
       n_max = n_top         ! Number of unique evals - for even last eval is not degenerate
    End If
    
    ! Generate the trig factors
    ! The access pattern for omega is somewhat strange, so to avoid weird performance problems
    ! keep it private as calculating it is very cheap
    Call get_trigs( n, omega )
    
    ! Now calculate evals (except the zero eval) and hence the scaled first evec
    !$omp single
    Allocate( q_scaled( 1:n_max ) )
    !$omp end single
    Call get_scaled_evec( n, n_max, 1, n_top,  i_c, c, w_c, omega, q_scaled )
    
    ! Now generate the evecs apart from those corresponding to the zero eigenvalue
    ! and dot onto the scaled evec to give the generating row/column of the inverse matrix
    !$omp single
    Allocate( a_row( 0:n - 1 ) )
    !$omp end single
    Call get_generator( n, 1, n_top, omega, q_scaled, a_row )
    
    ! Now generate the inverse matrix
    !$omp do
    Do i_state = 0, n - 1
       Do j = 0, i_state - 1
          a_inv( j, i_state ) = a_row( n - i_state + j ) 
       End Do
       Do j = i_state, n - 1
          a_inv( j, i_state ) = a_row( j - i_state ) 
       End Do
    End Do
    !$omp end do

    !$omp end parallel
    
  End Subroutine calc_pseudo_inv_ord

  Subroutine calc_pseudo_inv_perm( n, i_c, c, w_c, i_p, a_inv )

    Use numbers_module, Only : wp
    
    Implicit None ( Type, External )

    Integer                         , Intent( In    ) :: n
    Integer   , Dimension( 1:      ), Intent( In    ) :: i_c
    Real( wp ), Dimension( 0:      ), Intent( In    ) :: c
    Real( wp ), Dimension( 1:      ), Intent( In    ) :: w_c
    Integer   , Dimension( 0:      ), Intent( In    ) :: i_p
    Real( wp ), Dimension( 0:,  0: ), Intent(   Out ) :: a_inv

    Real( wp ), Dimension( : ), Allocatable :: omega
    Real( wp ), Dimension( : ), Allocatable :: q_scaled
    Real( wp ), Dimension( : ), Allocatable :: a_row
    Real( wp ), Dimension( : ), Allocatable :: a_row_shift

    Integer :: n_top, n_max
    Integer :: j, i_state

    !$omp parallel default( none ) shared( n, i_c, c, w_c, i_p, a_inv, q_scaled, a_row ) &
    !$omp&                         private( omega, a_row_shift, n_top, n_max, j, i_state )
    
    ! Limits for even and odd cases
    If( Mod( n, 2 )  == 0 ) Then
       n_top = ( n - 2 ) / 2 ! Number of pairs of degenerate evals excluding zero eval
       n_max = n_top + 1     ! Number of unique evals - for even last eval is not degenerate
    Else
       n_top = ( n - 1 ) / 2 ! Number of pairs of degenerate evals excluding zero eval
       n_max = n_top         ! Number of unique evals - for even last eval is not degenerate
    End If
    
    ! Generate the trig factors
    ! The access pattern for omega is somewhat strange, so to avoid weird performance problems
    ! keep it private as calculating it is very cheap
    Call get_trigs( n, omega )

    ! Now calculate evals (except the zero eval) and hence the scaled first evec
    !$omp single
    Allocate( q_scaled( 1:n_max ) )
    !$omp end single
    Call get_scaled_evec( n, n_max, 1, n_top,  i_c, c, w_c, omega, q_scaled )

    ! Now generate the evecs apart from those corresponding to the zero eigenvalue
    ! and dot onto the scaled evec to give the generating row/column of the inverse matrix
    !$omp single
    Allocate( a_row( 0:n - 1 ) )
    !$omp end single
    Call get_generator( n, 1, n_top, omega, q_scaled, a_row )

    ! Now generate the inverse matrix by the appropriate permutations
    ! of the generating row/column
    Allocate( a_row_shift( 0:n - 1 ) )
    !$omp do
    Do i_state = 0, n - 1
       ! Column permutation is a shift in the row
       Do j = 0, i_p( i_state ) - 1
          a_row_shift( j ) = a_row( n - i_p( i_state ) + j ) 
       End Do
       Do j = i_p( i_state ), n - 1
          a_row_shift( j ) = a_row( j - i_p( i_state ) ) 
       End Do
       ! Row permutation for the final result
       Do j = 0, n - 1
          a_inv( j, i_state ) = a_row_shift( i_p( j ) )
       End Do
    End Do
    !$omp end do

    !$omp end parallel
    
  End Subroutine calc_pseudo_inv_perm

  Subroutine calc_pseudo_inv_perm_mpi( n, start, finish, i_c, c, w_c, i_p, a_inv )

    Use numbers_module, Only : wp
    
    Implicit None ( Type, External )

    Integer                             , Intent( In    ) :: n
    Integer                             , Intent( In    ) :: start
    Integer                             , Intent( In    ) :: finish
    Integer   , Dimension( 1:          ), Intent( In    ) :: i_c
    Real( wp ), Dimension( 0:          ), Intent( In    ) :: c
    Real( wp ), Dimension( 1:          ), Intent( In    ) :: w_c
    Integer   , Dimension( 0:          ), Intent( In    ) :: i_p
    Real( wp ), Dimension( 0:,  start: ), Intent(   Out ) :: a_inv

    Real( wp ), Dimension( : ), Allocatable :: omega
    Real( wp ), Dimension( : ), Allocatable :: q_scaled
    Real( wp ), Dimension( : ), Allocatable :: a_row
    Real( wp ), Dimension( : ), Allocatable :: a_row_shift

    Integer :: n_top, n_max
    Integer :: j, i_state

    !$omp parallel default( none ) shared( n, start, finish, i_c, c, w_c, i_p, a_inv, q_scaled, a_row ) &
    !$omp&                          private( omega, a_row_shift, n_top, &
    !$omp&                                   n_max, j, i_state )
    
    ! Limits for even and odd cases
    If( Mod( n, 2 )  == 0 ) Then
       n_top = ( n - 2 ) / 2 ! Number of pairs of degenerate evals excluding zero eval
       n_max = n_top + 1     ! Number of unique evals - for even last eval is not degenerate
    Else
       n_top = ( n - 1 ) / 2 ! Number of pairs of degenerate evals excluding zero eval
       n_max = n_top         ! Number of unique evals - for even last eval is not degenerate
    End If
    
    ! Generate the trig factors
    ! The access pattern for omega is somewhat strange, so to avoid weird performance problems
    ! keep it private as calculating it is very cheap
    Call get_trigs( n, omega )

    ! Now calculate evals (except the zero eval) and hence the scaled first evec
    !$omp single
    Allocate( q_scaled( 1:n_max ) )
    !$omp end single
    Call get_scaled_evec( n, n_max, 1, n_top,  i_c, c, w_c, omega, q_scaled )

    ! Now generate the evecs apart from those corresponding to the zero eigenvalue
    ! and dot onto the scaled evec to give the generating row/column of the inverse matrix
    !$omp single
    Allocate( a_row( 0:n - 1 ) )
    !$omp end single
    Call get_generator( n, 1, n_top, omega, q_scaled, a_row )

    ! Now generate the inverse matrix by the appropriate permutations
    ! of the generating row/column
    Allocate( a_row_shift( 0:n - 1 ) )
    !$omp do
    Do i_state = start, finish
       ! Column permutation is a shift in the row
       Do j = 0, i_p( i_state ) - 1
          a_row_shift( j ) = a_row( n - i_p( i_state ) + j ) 
       End Do
       Do j = i_p( i_state ), n - 1
          a_row_shift( j ) = a_row( j - i_p( i_state ) ) 
       End Do
       ! Row permutation for the final result
       Do j = 0, n - 1
          a_inv( j, i_state ) = a_row_shift( i_p( j ) )
       End Do
    End Do
    !$omp end do

    !$omp end parallel
    
  End Subroutine calc_pseudo_inv_perm_mpi

  Subroutine get_trigs( n, omega )

    ! Generate the trig factors

    Use numbers_module, Only : wp
    
    Implicit None ( Type, External )
    
    Integer                   ,              Intent( In    ) :: n
    Real( wp ), Dimension( : ), Allocatable, Intent(   Out ) :: omega

    Real( wp ), Parameter :: pi = 4.0_wp * Atan( 1.0_wp )

    Real( wp ) :: dTheta

    Integer :: i

    dTheta =  ( 2.0_wp * pi / n )
    Allocate( omega( 0:n - 1 ) )
    omega( 0 ) = 1.0_wp
    ! The access pattern for omega is somewhat strange, so to avoid weird performance problems
    ! keep it private as calculating it is very cheap
    Do i = 1, n / 2
       omega( i     ) = Cos( i * dTheta )
       omega( n - i ) = omega( i )
    End Do
    
  End Subroutine get_trigs

  Subroutine get_scaled_evec( n, n_max, first, last, i_c, c, w_c, omega, q_scaled )

    ! Calculate scaled first row of evecs

    Use numbers_module, Only : wp, li
    
    Implicit None ( Type, External )
    
    Integer                         , Intent( In    ) :: n
    Integer                         , Intent( In    ) :: n_max
    Integer                         , Intent( In    ) :: first
    Integer                         , Intent( In    ) :: last
    Integer   , Dimension( 1:      ), Intent( In    ) :: i_c
    Real( wp ), Dimension( 0:      ), Intent( In    ) :: c
    Real( wp ), Dimension( 1:      ), Intent( In    ) :: w_c
    Real( wp ), Dimension( 0:      ), Intent( In    ) :: omega
    Real( wp ), Dimension( 1:      ), Intent(   Out ) :: q_scaled

    Real( wp ) :: norm_fac
    Real( wp ) :: lambda

    Integer( li ) :: ind

    Integer :: i_state, i_coeff
    
    ! Now calculate evals (except the zero eval) and hence the scaled first evec
    norm_fac = 2.0_wp * Sqrt( 1.0_wp / ( 2 * n ) )
    !$omp do
    Do i_state = first, last
       lambda = c( 0 )
       Do i_coeff = 1, Ubound( c, Dim = 1 )
          ! Should do this all with long integers to avoid overflow. Unlikely but ...
          ind = Mod( Int( i_c( i_coeff ), li ) * Int( i_state, li ), Int( n, li ) )
          lambda = lambda + w_c( i_coeff ) * c( i_coeff ) * Real( omega( ind ), Kind = wp )
       End Do
       q_scaled( i_state ) = norm_fac / lambda
    End Do
    !$omp end do
    ! Even end case
    If( Mod( n, 2 ) == 0 ) Then
       !$omp single
       lambda = c( 0 )
       Do i_coeff = 1, Ubound( c, Dim = 1 )
          lambda = lambda + w_c( i_coeff ) * c( i_coeff ) * ( 1 - 2 * Mod( i_c( i_coeff ), 2 ) )
       End Do
       norm_fac = Sqrt( 1.0_wp / n )
       q_scaled( n_max ) = norm_fac / lambda
       !$omp end single 
    End If

  End Subroutine get_scaled_evec

  Subroutine get_generator( n, first, last, omega, q_scaled, a_row )

    ! Calculate the generator row/column (as final matrix is symmetric)

    Use numbers_module, Only : wp, li
    
    Implicit None ( Type, External )
    
    Integer                    , Intent( In    ) :: n
    Integer                    , Intent( In    ) :: first
    Integer                    , Intent( In    ) :: last
    Real( wp ), Dimension( 0: ), Intent( In    ) :: omega
    Real( wp ), Dimension( 1: ), Intent( In    ) :: q_scaled
    Real( wp ), Dimension( 0: ), Intent(   Out ) :: a_row

    Real( wp ) :: norm_fac
    Real( wp ) :: fac1, fac2
    Real( wp ) :: tmp
    Real( wp ) :: sign

    Integer( li ) :: ind

    Integer :: n_max
    Integer :: j, i_state

    fac1 = 2.0_wp * Sqrt( 1.0_wp / ( 2 * n ) )
    fac2 = Sqrt( 1.0_wp / n )
    If( Mod( n, 2 ) /= 0 ) Then
       ! Odd case
       !$omp do
       Do j = 0, n - 1
          tmp = 0.0_wp
          Do i_state = first, last
             ! Should do this all with long integers to avoid overflow. Unlikely but ...
             ind = Mod( Int( i_state, li ) * Int( j, li ), Int( n, li ) )
             tmp = tmp +  omega( ind ) * q_scaled( i_state )
          End Do
          norm_fac = fac1
          tmp = tmp * norm_fac
          a_row( j ) = tmp
       End Do
       !$omp end do
    Else
       ! Even case
       n_max = Ubound( q_scaled, Dim = 1 )
       !$omp do
       Do j = 0, n - 1
          tmp = 0.0_wp
          Do i_state = first, last
             ! Should do this all with long integers to avoid overflow. Unlikely but ...
             ind = Mod( Int( i_state, li ) * Int( j, li ), Int( n, li ) )
             tmp = tmp + omega( ind ) * q_scaled( i_state )
          End Do
          norm_fac = fac1
          tmp = tmp * norm_fac
          ! Last component of evec for even case
          norm_fac = fac2
          sign = 1 - 2 * Mod( j, 2 )
          tmp = tmp + sign * norm_fac * q_scaled( n_max )
          a_row( j ) = tmp
       End Do
       !$omp end do
    End If

  End Subroutine get_generator

End Module circulant_solver_module

