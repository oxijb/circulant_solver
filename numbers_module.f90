Module numbers_module

  !! Little module to set the precision of real numbers that we shall use

  Use, Intrinsic :: iso_fortran_env, Only : real64, int64
  
  Implicit None 

  Integer, Parameter, Public :: wp = real64
  Integer, Parameter, Public :: li = int64
  
  Private
  
End Module numbers_module

