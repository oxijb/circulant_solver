Program circ2

  Use, Intrinsic :: iso_fortran_env, Only : stdout => output_unit, stdin => input_unit

!$  Use omp_lib, Only : omp_get_max_threads
  
  Use numbers_module         , Only : wp, li
  Use circulant_solver_module, Only : sten_to_list, &
       calc_pseudo_inv_diag, calc_pseudo_inv_ord, calc_pseudo_inv_perm, calc_pseudo_inv_perm_mpi

  Implicit None ( Type, External )

  Logical, Parameter :: with_diag = .False.
!!$  Logical, Parameter :: with_diag = .True.
  
  Real( wp ), Parameter :: sten_val = - 1.0_wp / 6.0_wp

  Character( Len = * ), Parameter :: err_fmt = '( "-----------> ", a, t60, g12.6, / )'

  Real( wp ), Dimension( -1:1, -1:1, -1:1 ) :: sten
  
  Real( wp ), Dimension( :, : ), Allocatable :: A
  Real( wp ), Dimension( :, : ), Allocatable :: A_inv_ord
  Real( wp ), Dimension( :, : ), Allocatable :: A_copy

  Real( wp ), Dimension( : ), Allocatable :: b
  Real( wp ), Dimension( : ), Allocatable :: b_ord
  Real( wp ), Dimension( : ), Allocatable :: x
  Real( wp ), Dimension( : ), Allocatable :: x_ord
  Real( wp ), Dimension( : ), Allocatable :: coeff
  Real( wp ), Dimension( : ), Allocatable :: w_coeff

  Integer, Dimension( : ), Allocatable :: i_coeff
  Integer, Dimension( : ), Allocatable :: i_perm

  Integer, Dimension( 1:3 ) :: n_mu
  Integer, Dimension( 1:3 ) :: ig
  Integer, Dimension( 1:3 ) :: ig_sten
  Integer, Dimension( 1:3 ) :: shift

  Integer( li ) :: start, finish, rate
  
  Integer :: n
  Integer :: n_coeff
  Integer :: i_sten
  Integer :: i, is, isign

!$ Write( *, * ) 'Threads ', omp_get_max_threads()
  
  Write( stdout, * ) 'n_mu'
  Read ( stdin , * ) n_mu

  n = Product( n_mu )
  Write( stdout, * ) 'n = ', n

  Allocate( a( 0:n - 1, 0:n - 1 ) )
  Allocate( a_copy( 0:n - 1, 0:n - 1 ) )

  Allocate( b( 0:n - 1 ) )
  Allocate( x( 0:n - 1 ) )

  Call Random_number( b )
  b = b - Sum( b ) / n
  Write( stdout, * ) 'Sum RHS ', Sum( b )

  Allocate( b_ord( 0:n - 1 ) )
  b_ord = b

  a = 0.0_wp
  Do i = 0, n - 1

     a( i, i ) = 1.0_wp

     ig( 3 ) = i / ( n_mu( 1 ) * n_mu( 2 ) )
     ig( 2 ) = ( i - ig( 3 ) * ( n_mu( 1 ) * n_mu( 2 ) ) ) / ( n_mu( 1 ) )
     ig( 1 ) = Mod( i, n_mu( 1 ) )

     shift = [ 1, 0, 0 ]
     Do is = 1, 3
        Do isign = -1, 1, 2
           ig_sten = ig + isign * shift
           i_sten = ig_sten( 1 ) + n_mu( 1 ) * ig_sten( 2 ) + n_mu( 1 ) * n_mu( 2 ) * ig_sten( 3 )
           i_sten = Modulo( i_sten, n )
           a( i, i_sten ) = sten_val
        End Do
        shift = Cshift( shift, -1 )
     End Do

  End Do

  a_copy = a

  sten = 0.0_wp
  sten( 0, 0, 0 ) = 1.0_wp
  sten( -1,  0,  0 ) = sten_val
  sten( +1,  0,  0 ) = sten_val
  sten(  0, -1,  0 ) = sten_val
  sten(  0, +1,  0 ) = sten_val
  sten(  0,  0, -1 ) = sten_val
  sten(  0,  0, +1 ) = sten_val

  Call sten_to_list( n_mu, [ -1, -1, -1 ], sten, n_coeff, i_coeff, coeff, w_coeff )

  ! Full diag method in natural order - slow!! But good reference for checking results
  If( with_diag ) Then
     Call system_clock( start, rate )
     Call calc_pseudo_inv_diag( n, n_coeff, i_coeff, coeff, a )
     Call system_clock( finish, rate )
     Write( stdout, * ) 'T3 = ', Real( finish - start, wp ) / rate
     Call system_clock( start, rate )
     x = Matmul( a, b )
     Call system_clock( finish, rate )
     Write( stdout, * ) 'T4 = ', Real( finish - start, wp ) / rate
     Write( stdout, err_fmt ) 'Error for diag ', Maxval( Abs( Matmul( a_copy, x ) - b ) )
  End If

  ! Generator row method, in natural order
  b = b_ord
  Call random_number( a  )
  Call system_clock( start, rate )
  Call calc_pseudo_inv_ord( n, i_coeff, coeff, w_coeff, a )
  Call system_clock( finish, rate )
  Write( stdout, * ) 'TB = ', Real( finish - start, wp ) / rate
  Call system_clock( start, rate )
  x = Matmul( a, b )
  Call system_clock( finish, rate )
  Write( stdout, * ) 'TC = ', Real( finish - start, wp ) / rate
  Write( stdout, err_fmt ) 'Error for generator row, natural ordering ', Maxval( Abs( Matmul( a_copy, x ) - b ) )
  ! Store the natural order results
  a_inv_ord = a
  x_ord = x

  ! Permute the RHS
  Allocate( i_perm( 0:n - 1 ) )
  i_perm = [ ( i, i = n - 1, 0, -1 ) ]
  Call swapsies( i_perm )

  ! Generator row method, with permutation
  b = b_ord( i_perm )
  Call system_clock( start, rate )
  Call calc_pseudo_inv_perm( n, i_coeff, coeff, w_coeff, i_perm, a )
  Call system_clock( finish, rate )
  Write( stdout, * ) 'TD = ', Real( finish - start, wp ) / rate
  Call system_clock( start, rate )
  x = Matmul( a, b )
  Call system_clock( finish, rate )
  Write( stdout, * ) 'TE = ', Real( finish - start, wp ) / rate
  Write( stdout, * ) 'Error in permuted x', Maxval( Abs( x - x_ord( i_perm ) ) )
  Write( stdout, * ) 'Error in permuted inverse ', Maxval( Abs( a_inv_ord( i_perm, i_perm ) - a ) )
  Write( stdout, err_fmt ) 'Error for generator row, permuted ', Maxval( Abs( Matmul( a_copy( i_perm, i_perm ), x ) - b ) )
  
  Do i = 0, n - 1
     Write( 100, '( 2( i5, 1x ), 3( f10.7, 1x ), f18.15 )' ) &
          i, i_perm( i ), x_ord( i ), x_ord( i_perm( i ) ), x( i ), Abs( x( i ) - x_ord( i_perm( i ) ) )
  End Do

!!$  Write( *, * ) a( :, 2 )
!!$  Write( *, * ) a( :, 3 )
!!$  Write( *, * )

  b = b_ord( i_perm )
  Call system_clock( start, rate )
  Call calc_pseudo_inv_perm_mpi( n, 2, 3, i_coeff, coeff, w_coeff, i_perm, a )
  Call system_clock( finish, rate )
  Write( stdout, * ) 'TD = ', Real( finish - start, wp ) / rate
  Call system_clock( start, rate )
  x = Matmul( a, b )
  Call system_clock( finish, rate )
  Write( stdout, * ) 'TE = ', Real( finish - start, wp ) / rate
  Write( stdout, * ) 'Error in permuted x', Maxval( Abs( x - x_ord( i_perm ) ) )
  Write( stdout, * ) 'Error in permuted inverse ', Maxval( Abs( a_inv_ord( i_perm, i_perm ) - a ) )
  Write( stdout, err_fmt ) 'Error for generator row, permuted MPI ', Maxval( Abs( Matmul( a_copy( i_perm, i_perm ), x ) - b ) )

!!$  Write( *, * ) a( :, 0 )
!!$  Write( *, * ) a( :, 1 )
!!$  Write( *, * )

Contains

    Subroutine swapsies( i_perm )

    Use numbers_module, Only : wp

    Integer, Dimension( : ), Intent( InOut ) :: i_perm

    Real( wp ), Dimension( 1:2 ) :: r

    Integer, Dimension( 1:2 ) :: swap
    
    Integer :: tmp
    Integer :: n
    Integer :: i

    n = Size( i_perm )

    Do i = 1, 3 * n
       Call Random_number( r )
       swap = Int( r * n ) + 1
       tmp  = i_perm( swap( 1 ) )
       i_perm( swap( 1 ) ) = i_perm( swap( 2 ) )
       i_perm( swap( 2 ) ) = tmp
    End Do
    
  End Subroutine swapsies
  
End Program circ2
