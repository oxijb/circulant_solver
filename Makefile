PROG =	circ2

SRCS =	circ2.f90 main.f90 numbers_module.f90

OBJS =	circ2.o main.o numbers_module.o

LIBS =	-lopenblas

CC = cc
CFLAGS = -O
FC = f77
FFLAGS = -O
F90 = gfortran
#F90FLAGS = -O -g -std=f2018 -Wall -Wextra -fcheck=all -finit-real=snan -Wuse-without-only -ffpe-trap=invalid,zero,overflow,denormal
F90FLAGS = -O -g -std=f2018 -Wall -Wextra -fcheck=all -finit-real=snan -Wuse-without-only -ffpe-trap=invalid,zero,overflow,denormal -fopenmp
#F90FLAGS = -O3 -g -std=f2018 -Wall -Wextra -fopenmp
LDFLAGS = -g -fopenmp

all: $(PROG)

$(PROG): $(OBJS)
	$(F90) $(LDFLAGS) -o $@ $(OBJS) $(LIBS)

clean:
	rm -f $(PROG) $(OBJS) *.mod

.SUFFIXES: $(SUFFIXES) .f90

.f90.o:
	$(F90) $(F90FLAGS) -c $<

circ2.o: numbers_module.o
main.o: circ2.o numbers_module.o
